Title: DebConf23 starts today in Kochi On Sun 10 September 2023
Slug: debconf23-starts-today
Date: 2023-09-10 11:00
Author: The Debian Publicity Team
Tags: debconf, debconf23
Lang: en
Artwork: Debian
Status: published


[DebConf23](https://debconf23.debconf.org), the 24th annual [Debian Developer Conference](https://www.debconf.org),
is taking place in Kochi, India from September 10th to 17th, 2023.

Debian contributors from all over the world have come together at Infopark, Kochi
to participate and work in a conference exclusively run by volunteers.

Today the main conference starts with over 373 expected attendants and 92
scheduled activities, including 45-minute and 20-minute talks, Bird of a Feather
("BoF") team meetings, workshops, a job fair, as well as a variety of other
events.

The full [schedule](https://debconf23.debconf.org/schedule/) is updated each
day, including activities planned ad-hoc by attendees over the course of the
conference.

If you would like to engage remotely, you can follow the **video streams**
available from the DebConf23 website for the events happening in the three talk
rooms: _Anamudi_, _Kuthiran_ and _Ponmudi_. Or you can join the conversations
happening inside the talk rooms via the [OFTC IRC network](https://www.oftc.net)
in the **#debconf-anamudi**,  **#debconf-kuthiran**, and the  **#debconf-ponmudi**
channels. Please also join us in the **#debconf** channel for common discussions
related to DebConf.

You can also follow the live coverage of news about DebConf23 provided by our
[micronews service](https://micronews.debian.org) or the @debian profile on your
favorite social network.

DebConf is committed to a safe and welcoming environment for all participants.
Please see our Code of Conduct page on the  DebConf23 website for more
information on this.

Debian thanks the commitment of numerous sponsors to support DebConf23,
particularly our Platinum Sponsors: **Infomaniak**, **Proxmox** and **Siemens**.


![DebConf23 logo](|static|/images/debconf23-sponsors-banner_1_800x450.png)
~                                                                            
